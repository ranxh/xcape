# Created by: test
# $FreeBSD$

PORTNAME=		xcape
PORTVERSION=		1.2
DISTVERSIONPREFIX=	v
PORTREVISION=		1
CATEGORIES=		x11

USE_GITHUB=		yes
GH_ACCOUNT=		alols

MAINTAINER=		ports@FreeBSD.org
COMMENT=		Key Swapping for XOrg

LICENSE=		GPLv3

USE_XORG=		x11 xtst

USES=			compiler pkgconfig

CFLAGS =		-g
CFLAGS +=		`pkg-config --cflags xtst x11`
LDFLAGS +=		`pkg-config --libs xtst x11`
LDFLAGS +=		-pthread

do-build:
		cd ${WRKSRC}; \
			${CC} ${CFLAGS} -o ${PORTNAME} ${PORTNAME}.c ${LDFLAGS}

do-install:
		${INSTALL_PROGRAM} ${WRKSRC}/${PORTNAME} ${STAGEDIR}${PREFIX}/bin/${PORTNAME}
		${INSTALL_MAN} ${WRKSRC}/${PORTNAME}.1 ${STAGEDIR}${MANPREFIX}/man/man1/${PORTNAME}.1

.include <bsd.port.mk>
